/**
 * Mixins are a flexible way to distribute reusable functionalities for Vue components.
 * https://vuejs.org/v2/guide/mixins.html
 * The following mixin will only be imported to ALL components in this project (even the 3rd party components)
 * all the following methods and variables will be available to ALL components as this mixin will be imported in main.js
 */

const API_URL = 'http://localhost:8002';
const GlobalMixin = ({
    data(){
        return {
            API_URL
        }
    }
});

export default GlobalMixin
