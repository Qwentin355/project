<?php
/***
 * Class weather
 * This class is meant to store data about a student - Data Object aka Entity aka Model
 * Put another way, this class/object would represent the data from a single row in a database table
 */


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="students")
 */
class weather
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id; //adding a question mark in front of the type makes the property nullable

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $userName; //saskpolytech username usually lastname with 4 random digits

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    protected $address;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    protected $phone;


    /*****************
     * GETTER and SETTERS
     */


    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }


    /**
	* @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName(string $userName): void
    {
        $this->userName = $userName;
    }

   //getters and setters for the protected and private properties

}

//WE NEVER WANT the closing php tag in a CLASS declaration files


























