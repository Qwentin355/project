# cweb-vuejs

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Adds Routing support to project - creates folders: 'router' and 'views'
```
vue add @vue/cli-plugin-router
```


### PHP Webserver - in terminal, navigate to folder where getstudentjson.php exists
```
php -S localhost:8001
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
