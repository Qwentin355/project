<?php
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
require_once '../init-db.php';
require_once '../entities/Subject.php';
require_once 'SubjectController.php';
header("Access-Control-Allow-Origin: *");

//PHP natively handles get an post request, but not delete and put requests
//     so we have to add code to parse the string into an assoc array
$dataFile = file_get_contents('php://input'); //get content of request file -usually in JSON format

//if request data file is not empty decode the JSON into a PHP associative array - otherwise the data is in $_REQUEST super-global
//FYI the $_REQUEST super global is a merging of both the $_GET and $_POST
$requestData = !empty($dataFile)? json_decode($dataFile,true) : $_REQUEST;

/**
 * ReSTful application - https://restfulapi.net/
 * for us this means we will be using different request methods to the same URL for different results/actions
 * You have seen GET and POST request methods
 * We are introducing 2 new request methods PUT and DELETE
 * PUT and DELETE cannot easily be sent throw a browser(needs some javascript/html)i
 * */
$resultToEncode= '';
switch ($_SERVER['REQUEST_METHOD']){
    case 'GET': //handle GET Request by calling the  controller's getSubjects function to GET subjects from the database
        // - send the entity manager to perform database queries and the data from the request
        $resultToEncode = SubjectController::getSubjects($entityManager, $requestData);
        break;
    case 'POST': //handle POST Request by call controller's postSubject function to ADD a subject to the database
        // - send the entity manager to perform database queries, the data from the request and a new subject entity
        $resultToEncode = SubjectController::postSubjects($entityManager, $requestData, new Subject());
        break;
    case 'PUT'://handle PUT Request by call controller's putSubject function to UPDATE a subject in the database
        // - send the entity manager to perform database queries, the data from the request and
        //-- an existing subject from the database with the id provided
        $subject = isset($requestData['id']) ? $entityManager->find(Subject::class,$requestData['id']): null;
        $resultToEncode = SubjectController::putSubject($entityManager, $requestData, $subject);
        break;
    case 'DELETE'://handle DELETE Request by call controller's deleteCUeCard function to DELETE a subject in the database
        // - send the entity manager to perform database queries, the data from the request and
        //-- an existing subject from the database with the id provided

        $subject = isset($requestData['id']) ? $entityManager->find(Subject::class,$requestData['id']): null;
        $resultToEncode = SubjectController::deleteSubject($entityManager, $requestData, $subject);
        break;
    case 'OPTIONS'://handle OPTIONS Request - used as a CORS preflight security check request
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS#Examples_of_access_control_scenarios
        // - send back the allowed methods, headers and max age
        http_response_code(204);// No Content
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS'); //allow the supported methods
        header('Access-Control-Allow-Headers: Content-type' );//allow Content-type header in request
        header('Access-Control-Max-Age: 86400');//1 day
        break;
    default://All other request methods are not supported/allowed by our api
        //set the status code to let the browser/postman know that the request method is not allowed
        http_response_code(405);// method not allowed
}

//this bypass CORS security, NOTE: In industry this is done on the webserver by and admin - NOT IN CODE
header('Access-Control-Allow-Origin:*');

//FYI: in order for an ajax request to send cookies, when CORS security is active, the server has to return the following headers
//header('Access-Control-Allow-Origin: http://localhost:8080'); //set the url to the actual vuejs project url and port number
//header('Access-Control-Allow-Credentials: true'); //will eventually need cookies in order to use PHP sessions

//By default webservers serve up html text files - we need to tell the browser that this is a JSON text file
header('Content-type:application/json');

//uncomment the line below to add a delay to the response so the loading screen is visible in the vue js project
sleep(2);

//if response code is not '204 no content'
if(http_response_code()!=204) {

    //instantiate an instance of the serializer
    $serializer = new Serializer([new ObjectNormalizer()],[new JsonEncoder()]);

    // echo out the serialized subject array in json format
    echo $serializer->serialize($resultToEncode, 'json');
}



