<?php

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

require_once '../init-db.php';
require_once '../entities/CityRating.php';
require_once 'RatingController.php';

$dataFile = file_get_contents('php://input');
$requestData = !empty($dataFile)? json_decode($dataFile,true) : $_REQUEST;

$resultToEncode= '';

switch ($_SERVER['REQUEST_METHOD']){
    case 'GET':
            $resultToEncode = RatingController::getAverage($entityManager, $requestData);
        break;
    case 'POST':
        $resultToEncode = RatingController::InsertRating($entityManager, $requestData, new CityRating());
        break;
    case 'PUT':
        $Rating = isset($requestData['id']) ? $entityManager->find(CityRating::class,$requestData['id']): null; //finds the entry
        $resultToEncode = RatingController::updateRating($entityManager, $requestData, $Rating );
        break;
    case 'OPTIONS':
        http_response_code(204);// No Content
        header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS'); //allow the supported methods
        header('Access-Control-Allow-Headers: Content-type' );//allow Content-type header in request
        header('Access-Control-Max-Age: 86400');//1 day
        break;
    default:
        http_response_code(405);// method not allowed
}

//this bypass CORS security, NOTE: In industry this is done on the webserver by and admin - NOT IN CODE
header('Access-Control-Allow-Origin:*');
//By default webservers serve up html text files - we need to tell the browser that this is a JSON text file
header('Content-type:application/json');

//instantiate an instance of the serializer
$serializer = new Serializer([new ObjectNormalizer()],[new JsonEncoder()]);

// echo out the serailized result array in json format
echo $serializer->serialize($resultToEncode, 'json');
