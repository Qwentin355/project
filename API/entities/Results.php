<?php


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;
/**
 * @ORM\Entity
 * @ORM\Table(name="Results")
 */

class Results
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $resultID;

    /**
     * @ORM\Column(type="string")
     */
    protected $Cuecard;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $Correct;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $Incorrect;

    /*****************
     * GETTER and SETTERS
     */

    /**
     * @return int
     */
    public function subjectID(): int
    {
        return $this->subjectID();
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->Author;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->Subject;
    }
}