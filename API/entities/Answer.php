<?php
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\Table(name="answer")
 */
class Answer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=500)
     */
    protected $answer;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $correct;

  /**
   * * @ORM\Column(type="integer")
   */
    protected $CueCardID;



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @return int
     */
    public function getCardID(): int
    {
        return $this->CueCardID;
    }
    /**
     * @return string|null
     */
    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    /**
     * @return boolean
     */
    public function getCorrect(): ?string
    {
        return $this->correct;
    }

    /**
     * @param boolean $correct
     */
    public function setCorrect(string $correct): void
    {
        $this->correct = $correct;
    }

    /**
     * @param string $answer
     */
    public function setAnswer(string $answer): void
    {
        $this->answer = $answer;
    }
    /**
     * @param int $CueCardID
     */
    public function setCueCardID(string $CueCardID): void
    {
        $this->CueCardID = $CueCardID;
    }



}