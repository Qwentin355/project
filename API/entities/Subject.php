<?php
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;
/**
 * @ORM\Entity
 * @ORM\Table(name="Subjects")
 */

class Subject
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $subjectID;

    /**
     * @ORM\Column(type="string")
     */
    protected  $Author;

    /**
     * @ORM\Column(type="string")
     */
    protected  $Subject;


    /*****************
     * GETTER and SETTERS
     */

    /**
     * @return int
     */
    public function subjectID(): int
    {
        return $this->subjectID;
    }
    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->Author;
    }
    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->Subject;
    }

    /**
     * @param string $Author
     */
    public function setAuthor(string $Author)
    {
        $this->Author = $Author;
    }
    /**
     * @param string $Subject
     */
    public function setSubject(string $Subject)
    {
        $this->Subject = $Subject;
    }
}